# gfortran-ci

Docker image based on gcc:9 which includes CMake, Doxygen, Graphviz, Gnuplot, and LaTeX.

The original intent of this project is to provide a build/test/document/package environment for SOFIRE II (https://gitlab.com/apthorpe/sofire2) however this image should be suitable for use with standalone serial (non-HPC) Fortran applications, for example when recovering and modernizing legacy applications.